'use strict'

/**
 * Завдання
 * Намалювати на сторінці коло за допомогою параметрів, які введе користувач. Завдання має бути виконане на чистому Javascript
 * без використання бібліотек типу jQuery або React.
 *
 * Технічні вимоги:
 * При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло". Дана кнопка повинна бути єдиним контентом у тілі HTML документа,
 * решта контенту потрібно створити і додати на сторінку за допомогою Javascript.
 * При натисканні на кнопку "Намалювати коло" показувати одне поле введення - діаметр кола. При натисканні на кнопку "Намалювати"
 * створити на сторінці 100 кіл (10*10) випадкового кольору. При кліку на конкретне коло - це коло має зникати, у своїй порожнє місце
 * заповнюватися, тобто інші кола зрушуються вліво.
 * У вас може виникнути бажання поставити обробник події на кожне коло для його зникнення. Це неефективно, так не треба робити.
 * На всю сторінку має бути лише один обробник подій, який це робитиме.
 * */

document.querySelector('.btn').addEventListener('click', function() {

    if (!document.querySelector('#circlesize')) {
        document.querySelector('.btn').insertAdjacentHTML('afterend', '<input id="circlesize">');

    } else if (document.querySelector('#circlesize').value) {

        let container = document.createElement('div');
        let circleSize = +document.querySelector('#circlesize').value;
        document.querySelector('#circlesize').value = '';
        container.style.cssText = 'display: flex; flex-wrap: wrap; width: 95vw';
        container.classList.add('container');
        document.querySelector('#circlesize').insertAdjacentElement('afterend', container);

        function getRandomColor() {
            let color = '#';
            let symbols = '0123456789ABCDEFGH'
            for (let i = 0; i < 6; i++) {
                color += symbols[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        for (let i = 0; i < 100; i++) {
            let circle = document.createElement('div');
            circle.classList.add('circle');
            circle.innerText = `${i + 1}`;
            circle.style.cssText = `width: ${circleSize}px;
                                    height: ${circleSize}px;
                                    line-height: ${circleSize}px;
                                    border-radius: 50%;
                                    background-color: ${getRandomColor()};
                                    margin: 5px;
                                    text-align: center;
                                    font-family: Arial;
                                    font size: 16px;
                                    font-weight: 700;
                                    user-select: none`;
            container.append(circle);
        }
    }
})

window.addEventListener('click', function (ev) {
        ev.target.classList.contains('circle') ? ev.target.remove() : false;
})



